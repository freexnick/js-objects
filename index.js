const result = {
  subjects: {
    javascript: 4,
    react: 7,
    python: 6,
    java: 3,
  },
  students: {
    jR: {
      personalInfo: {
        name: "Jean",
        lastName: "Reno",
        age: 26,
      },
      grades: {
        javascript: 62,
        react: 57,
        python: 88,
        java: 90,
      },
    },
    kD: {
      personalInfo: {
        name: "Klod",
        lastName: "Mone",
        age: 19,
      },
      grades: {
        javascript: 77,
        react: 52,
        python: 92,
        java: 67,
      },
    },
    vG: {
      personalInfo: {
        name: "Van",
        lastName: "Gogh",
        age: 21,
      },
      grades: {
        javascript: 51,
        react: 98,
        python: 65,
        java: 70,
      },
    },
    dS: {
      personalInfo: {
        name: "Dam",
        lastName: "Square",
        age: 36,
      },
      grades: {
        javascript: 82,
        react: 53,
        python: 80,
        java: 65,
      },
    },
  },
};
//calculating summary
result.students.jR.grades.summary =
  result.students.jR.grades.javascript +
  result.students.jR.grades.react +
  result.students.jR.grades.python +
  result.students.jR.grades.java;

result.students.kD.grades.summary =
  result.students.kD.grades.javascript +
  result.students.kD.grades.react +
  result.students.kD.grades.python +
  result.students.kD.grades.java;

result.students.vG.grades.summary =
  result.students.vG.grades.javascript +
  result.students.vG.grades.react +
  result.students.vG.grades.python +
  result.students.vG.grades.java;

result.students.dS.grades.summary =
  result.students.dS.grades.javascript +
  result.students.dS.grades.react +
  result.students.dS.grades.python +
  result.students.dS.grades.java;

//calculating avarage
const allSubjArray = [
  result.subjects.javascript,
  result.subjects.react,
  result.subjects.python,
  result.subjects.java,
];
result.students.jR.grades.average =
  result.students.jR.grades.summary / allSubjArray.length;

result.students.kD.grades.average =
  result.students.kD.grades.summary / allSubjArray.length;

result.students.vG.grades.average =
  result.students.vG.grades.summary / allSubjArray.length;

result.students.dS.grades.average =
  result.students.dS.grades.summary / allSubjArray.length;

//calculating points
result.students.jR.grades.points = {};
result.students.kD.grades.points = {};
result.students.vG.grades.points = {};
result.students.dS.grades.points = {};
//jr
if (result.students.jR.grades.javascript >= 91) {
  result.students.jR.grades.points.javascript = 4;
} else if (result.students.jR.grades.javascript >= 81) {
  result.students.jR.grades.points.javascript = 3;
} else if (result.students.jR.grades.javascript >= 71) {
  result.students.jR.grades.points.javascript = 2;
} else if (result.students.jR.grades.javascript >= 61) {
  result.students.jR.grades.points.javascript = 1;
} else if (result.students.jR.grades.javascript >= 51) {
  result.students.jR.grades.points.javascript = 0.5;
} else {
  result.students.jR.grades.points.javascript = 0;
}
if (result.students.jR.grades.react >= 91) {
  result.students.jR.grades.points.react = 4;
} else if (result.students.jR.grades.react >= 81) {
  result.students.jR.grades.points.react = 3;
} else if (result.students.jR.grades.react >= 71) {
  result.students.jR.grades.points.react = 2;
} else if (result.students.jR.grades.react >= 61) {
  result.students.jR.grades.points.react = 1;
} else if (result.students.jR.grades.react >= 51) {
  result.students.jR.grades.points.react = 0.5;
} else {
  result.students.jR.grades.points.react = 0;
}
if (result.students.jR.grades.python >= 91) {
  result.students.jR.grades.points.python = 4;
} else if (result.students.jR.grades.python >= 81) {
  result.students.jR.grades.points.python = 3;
} else if (result.students.jR.grades.python >= 71) {
  result.students.jR.grades.points.python = 2;
} else if (result.students.jR.grades.python >= 61) {
  result.students.jR.grades.points.python = 1;
} else if (result.students.jR.grades.python >= 51) {
  result.students.jR.grades.points.python = 0.5;
} else {
  result.students.jR.grades.points.python = 0;
}
if (result.students.jR.grades.java >= 91) {
  result.students.jR.grades.points.java = 4;
} else if (result.students.jR.grades.java >= 81) {
  result.students.jR.grades.points.java = 3;
} else if (result.students.jR.grades.java >= 71) {
  result.students.jR.grades.points.java = 2;
} else if (result.students.jR.grades.java >= 61) {
  result.students.jR.grades.points.java = 1;
} else if (result.students.jR.grades.java >= 51) {
  result.students.jR.grades.points.java = 0.5;
} else {
  result.students.jR.grades.points.java = 0;
}

//kD
if (result.students.kD.grades.javascript >= 91) {
  result.students.kD.grades.points.javascript = 4;
} else if (result.students.kD.grades.javascript >= 81) {
  result.students.kD.grades.points.javascript = 3;
} else if (result.students.kD.grades.javascript >= 71) {
  result.students.kD.grades.points.javascript = 2;
} else if (result.students.kD.grades.javascript >= 61) {
  result.students.kD.grades.points.javascript = 1;
} else if (result.students.kD.grades.javascript >= 51) {
  result.students.kD.grades.points.javascript = 0.5;
} else {
  result.students.kD.grades.points.javascript = 0;
}
if (result.students.kD.grades.react >= 91) {
  result.students.kD.grades.points.react = 4;
} else if (result.students.kD.grades.react >= 81) {
  result.students.kD.grades.points.react = 3;
} else if (result.students.kD.grades.react >= 71) {
  result.students.kD.grades.points.react = 2;
} else if (result.students.kD.grades.react >= 61) {
  result.students.kD.grades.points.react = 1;
} else if (result.students.kD.grades.react >= 51) {
  result.students.kD.grades.points.react = 0.5;
} else {
  result.students.kD.grades.points.react = 0;
}
if (result.students.kD.grades.python >= 91) {
  result.students.kD.grades.points.python = 4;
} else if (result.students.kD.grades.python >= 81) {
  result.students.kD.grades.points.python = 3;
} else if (result.students.kD.grades.python >= 71) {
  result.students.kD.grades.points.python = 2;
} else if (result.students.kD.grades.python >= 61) {
  result.students.kD.grades.points.python = 1;
} else if (result.students.kD.grades.python >= 51) {
  result.students.kD.grades.points.python = 0.5;
} else {
  result.students.kD.grades.points.python = 0;
}
if (result.students.kD.grades.java >= 91) {
  result.students.kD.grades.points.java = 4;
} else if (result.students.kD.grades.java >= 81) {
  result.students.kD.grades.points.java = 3;
} else if (result.students.kD.grades.java >= 71) {
  result.students.kD.grades.points.java = 2;
} else if (result.students.kD.grades.java >= 61) {
  result.students.kD.grades.points.java = 1;
} else if (result.students.kD.grades.java >= 51) {
  result.students.kD.grades.points.java = 0.5;
} else {
  result.students.kD.grades.points.java = 0;
}

//vG
if (result.students.vG.grades.javascript >= 91) {
  result.students.vG.grades.points.javascript = 4;
} else if (result.students.vG.grades.javascript >= 81) {
  result.students.vG.grades.points.javascript = 3;
} else if (result.students.vG.grades.javascript >= 71) {
  result.students.vG.grades.points.javascript = 2;
} else if (result.students.vG.grades.javascript >= 61) {
  result.students.vG.grades.points.javascript = 1;
} else if (result.students.vG.grades.javascript >= 51) {
  result.students.vG.grades.points.javascript = 0.5;
} else {
  result.students.vG.grades.points.javascript = 0;
}
if (result.students.vG.grades.react >= 91) {
  result.students.vG.grades.points.react = 4;
} else if (result.students.vG.grades.react >= 81) {
  result.students.vG.grades.points.react = 3;
} else if (result.students.vG.grades.react >= 71) {
  result.students.vG.grades.points.react = 2;
} else if (result.students.vG.grades.react >= 61) {
  result.students.vG.grades.points.react = 1;
} else if (result.students.vG.grades.react >= 51) {
  result.students.vG.grades.points.react = 0.5;
} else {
  result.students.vG.grades.points.react = 0;
}
if (result.students.vG.grades.python >= 91) {
  result.students.vG.grades.points.python = 4;
} else if (result.students.vG.grades.python >= 81) {
  result.students.vG.grades.points.python = 3;
} else if (result.students.vG.grades.python >= 71) {
  result.students.vG.grades.points.python = 2;
} else if (result.students.vG.grades.python >= 61) {
  result.students.vG.grades.points.python = 1;
} else if (result.students.vG.grades.python >= 51) {
  result.students.vG.grades.points.python = 0.5;
} else {
  result.students.vG.grades.points.python = 0;
}
if (result.students.vG.grades.java >= 91) {
  result.students.vG.grades.points.java = 4;
} else if (result.students.vG.grades.java >= 81) {
  result.students.vG.grades.points.java = 3;
} else if (result.students.vG.grades.java >= 71) {
  result.students.vG.grades.points.java = 2;
} else if (result.students.vG.grades.java >= 61) {
  result.students.vG.grades.points.java = 1;
} else if (result.students.vG.grades.java >= 51) {
  result.students.vG.grades.points.java = 0.5;
} else {
  result.students.vG.grades.points.java = 0;
}

//dS
if (result.students.dS.grades.javascript >= 91) {
  result.students.dS.grades.points.javascript = 4;
} else if (result.students.dS.grades.javascript >= 81) {
  result.students.dS.grades.points.javascript = 3;
} else if (result.students.dS.grades.javascript >= 71) {
  result.students.dS.grades.points.javascript = 2;
} else if (result.students.dS.grades.javascript >= 61) {
  result.students.dS.grades.points.javascript = 1;
} else if (result.students.dS.grades.javascript >= 51) {
  result.students.dS.grades.points.javascript = 0.5;
} else {
  result.students.dS.grades.points.javascript = 0;
}
if (result.students.dS.grades.react >= 91) {
  result.students.dS.grades.points.react = 4;
} else if (result.students.dS.grades.react >= 81) {
  result.students.dS.grades.points.react = 3;
} else if (result.students.dS.grades.react >= 71) {
  result.students.dS.grades.points.react = 2;
} else if (result.students.dS.grades.react >= 61) {
  result.students.dS.grades.points.react = 1;
} else if (result.students.dS.grades.react >= 51) {
  result.students.dS.grades.points.react = 0.5;
} else {
  result.students.dS.grades.points.react = 0;
}
if (result.students.dS.grades.python >= 91) {
  result.students.dS.grades.points.python = 4;
} else if (result.students.dS.grades.python >= 81) {
  result.students.dS.grades.points.python = 3;
} else if (result.students.dS.grades.python >= 71) {
  result.students.dS.grades.points.python = 2;
} else if (result.students.dS.grades.python >= 61) {
  result.students.dS.grades.points.python = 1;
} else if (result.students.dS.grades.python >= 51) {
  result.students.dS.grades.points.python = 0.5;
} else {
  result.students.dS.grades.points.python = 0;
}
if (result.students.dS.grades.java >= 91) {
  result.students.dS.grades.points.java = 4;
} else if (result.students.dS.grades.java >= 81) {
  result.students.dS.grades.points.java = 3;
} else if (result.students.dS.grades.java >= 71) {
  result.students.dS.grades.points.java = 2;
} else if (result.students.dS.grades.java >= 61) {
  result.students.dS.grades.points.java = 1;
} else if (result.students.dS.grades.java >= 51) {
  result.students.dS.grades.points.java = 0.5;
} else {
  result.students.dS.grades.points.java = 0;
}

//calculating GPA
const sumOfSubjCred =
  result.subjects.javascript +
  result.subjects.react +
  result.subjects.python +
  result.subjects.java;
//jR
result.students.jR.grades.gpa = {};
result.students.jR.grades.gpa.javascript =
  result.subjects.javascript * result.students.jR.grades.points.javascript;
result.students.jR.grades.gpa.react =
  result.subjects.react * result.students.jR.grades.points.react;
result.students.jR.grades.gpa.python =
  result.subjects.python * result.students.jR.grades.points.python;
result.students.jR.grades.gpa.java =
  result.subjects.java * result.students.jR.grades.points.java;

result.students.jR.grades.gpa.summary =
  result.students.jR.grades.gpa.javascript +
  result.students.jR.grades.gpa.react +
  result.students.jR.grades.gpa.python +
  result.students.jR.grades.gpa.java;

result.students.jR.grades.gpa.average =
  result.students.jR.grades.gpa.summary / sumOfSubjCred;

//kD
result.students.kD.grades.gpa = {};
result.students.kD.grades.gpa.javascript =
  result.subjects.javascript * result.students.kD.grades.points.javascript;
result.students.kD.grades.gpa.react =
  result.subjects.react * result.students.kD.grades.points.react;
result.students.kD.grades.gpa.python =
  result.subjects.python * result.students.kD.grades.points.python;
result.students.kD.grades.gpa.java =
  result.subjects.java * result.students.kD.grades.points.java;

result.students.kD.grades.gpa.summary =
  result.students.kD.grades.gpa.javascript +
  result.students.kD.grades.gpa.react +
  result.students.kD.grades.gpa.python +
  result.students.kD.grades.gpa.java;

result.students.kD.grades.gpa.average =
  result.students.kD.grades.gpa.summary / sumOfSubjCred;

//vG
result.students.vG.grades.gpa = {};
result.students.vG.grades.gpa.javascript =
  result.subjects.javascript * result.students.vG.grades.points.javascript;
result.students.vG.grades.gpa.react =
  result.subjects.react * result.students.vG.grades.points.react;
result.students.vG.grades.gpa.python =
  result.subjects.python * result.students.vG.grades.points.python;
result.students.vG.grades.gpa.java =
  result.subjects.java * result.students.vG.grades.points.java;

result.students.vG.grades.gpa.summary =
  result.students.vG.grades.gpa.javascript +
  result.students.vG.grades.gpa.react +
  result.students.vG.grades.gpa.python +
  result.students.vG.grades.gpa.java;

result.students.vG.grades.gpa.average =
  result.students.vG.grades.gpa.summary / sumOfSubjCred;

//dS
result.students.dS.grades.gpa = {};
result.students.dS.grades.gpa.javascript =
  result.subjects.javascript * result.students.dS.grades.points.javascript;
result.students.dS.grades.gpa.react =
  result.subjects.react * result.students.dS.grades.points.react;
result.students.dS.grades.gpa.python =
  result.subjects.python * result.students.dS.grades.points.python;
result.students.dS.grades.gpa.java =
  result.subjects.java * result.students.dS.grades.points.java;

result.students.dS.grades.gpa.summary =
  result.students.dS.grades.gpa.javascript +
  result.students.dS.grades.gpa.react +
  result.students.dS.grades.gpa.python +
  result.students.dS.grades.gpa.java;

result.students.dS.grades.gpa.average =
  result.students.dS.grades.gpa.summary / sumOfSubjCred;

//passing score
const studentsArray = [
  result.students.jR,
  result.students.kD,
  result.students.vG,
  result.students.dS,
];
result.subjects.passingGrade =
  (result.students.jR.grades.summary +
    result.students.kD.grades.summary +
    result.students.vG.grades.summary +
    result.students.dS.grades.summary) /
  (allSubjArray.length * studentsArray.length);

//student status
result.students.jR.grades.average > result.subjects.passingGrade
  ? (result.students.jR.grades.status = "red diploma")
  : (result.students.jR.grades.status = "public enemy");

result.students.kD.grades.average > result.subjects.passingGrade
  ? (result.students.kD.grades.status = "red diploma")
  : (result.students.kD.grades.status = "public enemy");

result.students.vG.grades.average > result.subjects.passingGrade
  ? (result.students.vG.grades.status = "red diploma")
  : (result.students.vG.grades.status = "public enemy");

result.students.dS.grades.average > result.subjects.passingGrade
  ? (result.students.dS.grades.status = "red diploma")
  : (result.students.dS.grades.status = "public enemy");

//https://youtu.be/GC5E8ie2pdM?t=63
result.students.bestStudents = {};

//by Gpa
let highestGpa = result.students.jR.grades.gpa.average;
result.students.bestStudents.byGpa = result.students.jR;

if (highestGpa < result.students.kD.grades.gpa.average) {
  highestGpa = result.students.kD.grades.gpa.average;
  result.students.bestStudents.byGpa = result.students.kD;
}
if (highestGpa < result.students.vG.grades.gpa.average) {
  highestGpa = result.students.vG.grades.gpa.average;
  result.students.bestStudents.byGpa = result.students.vG;
}
if (highestGpa < result.students.dS.grades.gpa.average) {
  highestGpa = result.students.dS.grades.gpa.average;
  result.students.bestStudents.byGpa = result.students.dS;
}

//by Age
if (
  result.students.jR.personalInfo.age &&
  result.students.jR.grades.average > result.students.kD.personalInfo.age &&
  result.students.kD.grades.average &&
  result.students.jR.personalInfo.age &&
  result.students.jR.grades.average > result.students.vG.personalInfo.age &&
  result.students.vG.grades.average &&
  result.students.jR.personalInfo.age &&
  result.students.jR.grades.average > result.students.dS.personalInfo.age &&
  result.students.dS.grades.average
) {
  result.students.bestStudents.byAge = result.students.jR;
} else if (
  result.students.kD.personalInfo.age &&
  result.students.kD.grades.average > result.students.jR.personalInfo.age &&
  result.students.jR.grades.average &&
  result.students.kD.personalInfo.age &&
  result.students.kD.grades.average > result.students.vG.personalInfo.age &&
  result.students.vG.grades.average &&
  result.students.kD.personalInfo.age &&
  result.students.kD.grades.average > result.students.dS.personalInfo.age &&
  result.students.dS.grades.average
) {
  result.students.bestStudents.byAge = result.students.kD;
} else if (
  result.students.vG.personalInfo.age &&
  result.students.vG.grades.average > result.students.jR.personalInfo.age &&
  result.students.jR.grades.average &&
  result.students.vG.personalInfo.age &&
  result.students.vG.grades.average > result.students.kD.personalInfo.age &&
  result.students.kD.grades.average &&
  result.students.vG.personalInfo.age &&
  result.students.vG.grades.average > result.students.dS.personalInfo.age &&
  result.students.dS.grades.average
) {
  result.students.bestStudents.byAge = result.students.vG;
} else {
  result.students.bestStudents.byAge = result.students.dS;
}
//by technology
const frontEndSubj = [result.subjects.javascript, result.subjects.react];

result.students.jR.grades.frontEndAvg =
  (result.students.jR.grades.javascript + result.students.jR.grades.react) /
  frontEndSubj.length;

result.students.kD.grades.frontEndAvg =
  (result.students.kD.grades.javascript + result.students.kD.grades.react) /
  frontEndSubj.length;

result.students.vG.grades.frontEndAvg =
  (result.students.vG.grades.javascript + result.students.vG.grades.react) /
  frontEndSubj.length;

result.students.dS.grades.frontEndAvg =
  (result.students.dS.grades.javascript + result.students.dS.grades.react) /
  frontEndSubj.length;

let highestFrontEndAvg = result.students.jR.grades.frontEndAvg;
result.students.bestStudents.byTech = result.students.jR;

if (highestFrontEndAvg < result.students.kD.grades.frontEndAvg) {
  highestFrontEndAvg = result.students.kD.grades.frontEndAvg;
  result.students.bestStudents.byTech = result.students.kD;
}
if (highestFrontEndAvg < result.students.vG.grades.frontEndAvg) {
  highestFrontEndAvg = result.students.vG.grades.frontEndAvg;
  result.students.bestStudents.byTech = result.students.vG;
}
if (highestFrontEndAvg < result.students.dS.grades.frontEndAvg) {
  highestFrontEndAvg = result.students.dS.grades.frontEndAvg;
  result.students.bestStudents.byTech = result.students.dS;
}

console.log(result);
